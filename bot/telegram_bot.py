from secret import TGTOKEN

import logging

from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove, Update
from telegram.ext import (
    Updater,
    CommandHandler,
    MessageHandler,
    Filters,
    ConversationHandler,
    CallbackContext,
)

from neuronet.process_img import process_img

from db.readImage import insert_values


logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)

logger = logging.getLogger(__name__)

LETSGO, PHOTO = range(2)


def start(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info(f"Пользователь {user.first_name} {user.last_name} зашел в чат.")
    reply_keyboard = [['Да!']]

    update.message.reply_text(
        'Привет! Я бот Нимбус. Могу отличить облака по фотографии.'
        'Если захочешь меня выключить, напиши /cancel.\n\n'
        'Начнем?',
        reply_markup=ReplyKeyboardMarkup(
            reply_keyboard, resize_keyboard=True,
        ),
    )
    return LETSGO


def letsgo(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    update.message.reply_text(
        'Скинь мне фото, я определю что на нем',
        reply_markup=ReplyKeyboardRemove(),
    )
    return PHOTO


def photo(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    photo_file = update.message.photo[-1].get_file()
    logger.info(f"Пользователь {user.first_name} {user.last_name} загрузил фото.")
    photo_file.download('user_photo.jpg')
    update.message.reply_text(
        'Обрабатываю...'
    )
    res, label_f = process_img('user_photo.jpg')
    insert_values()
    logger.info(res)
    update.message.reply_text(
        res
    )
    return PHOTO


def cancel(update: Update, context: CallbackContext) -> int:
    """Cancels and ends the conversation."""
    user = update.message.from_user
    logger.info(f"Пользователь {user.first_name} {user.last_name} вышел из чата.")
    update.message.reply_text(
        'До свидания!', reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END


def main() -> None:
    updater = Updater(TGTOKEN)
    dispatcher = updater.dispatcher
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start)],
        states={
            LETSGO: [MessageHandler(Filters.regex('^(Да!|Нет!)$'), letsgo)],
            PHOTO: [MessageHandler(Filters.photo, photo)],
        },
        fallbacks=[CommandHandler('cancel', cancel)],
    )


    dispatcher.add_handler(conv_handler)

    updater.start_polling()

    updater.idle()


if __name__ == '__main__':
    main()