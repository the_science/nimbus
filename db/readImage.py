import sqlite3
import sys

from create_table import con

def readImage(filename):
	# Функция открытия изображения в бинарном режиме
    try:
        fin = open(filename, "rb")
        img = fin.read()
        return img
        
    except (IOError, e):
        # В случае ошибки, выводим ее текст
        print ("Error %d: %s" % (e.args[0],e.args[1]))
        sys.exit(1)
 
    finally:
        if fin:
            # Закрываем подключение с файлом
            fin.close()
    return


def insert_values():
	try:
	    # Открываем базу данных
	    con = sqlite3.connect('db/data.db')
	    cur = con.cursor()

	    # Получаем бинарные данные нашего файла
	    data = readImage("user_photo.jpg")

	    # Конвертируем данные
	    binary = sqlite3.Binary(data)

	    # Готовим запрос в базу
	    cur.execute("INSERT INTO table_photos VALUES (?)", (binary,) )
	    
	    # Выполняем запрос
	    con.commit()    
	    
	# В случае ошибки выводим ее текст.
	except (sqlite3.Error, e):
	    if con:
	        con.rollback()
	        
	    print ("Error %s:" % e.args[0])
	    sys.exit(1)
	    
	finally:
	    if con:
	    # Закрываем подключение с базой данных
	        con.close()
	return