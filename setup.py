from setuptools import setup, find_packages

setup(
	name="neuronet",
	version="1.1",
	packages=find_packages(),
)