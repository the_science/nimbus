from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.image import load_img
from tensorflow.keras.preprocessing.image import img_to_array
from keras.applications.vgg16 import preprocess_input
from keras.applications.vgg16 import decode_predictions
from keras.applications.vgg16 import VGG16
import numpy as np

from tensorflow.keras.models import load_model

model = load_model('neuronet/model_saved.h5')


# Вызываем функцию загрузки user_photo и определения пренаджлежности изображения к тому или иному типу
def process_img(img_path):
    image = load_img(img_path, target_size=(224, 224))
    img = np.array(image)
    img = img / 255.0
    img = img.reshape(1,224,224,3)
    label = model.predict(img)

    if label[0][0] > 0.5:
        res = 'Это кучевое облако'
    else:
        res = 'Это перистое облако'
    print(f'конец функции')
    return res, label[0][0]

        
