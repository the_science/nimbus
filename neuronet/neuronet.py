from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
from keras import backend as K

# размер изображения 
img_width, img_height = 224, 224

# каталог данных для обучения
train_data_dir = 'neuronet/v_data/train'

# каталог данных для проверки
validation_data_dir = 'neuronet/v_data/test'

# количество изображений для тестирования
nb_train_samples = 386

# количество изображений для проверки
nb_validation_samples = 34

# количество эпох, в течении которых мы обучаем сеть
epochs = 30

# размер минивыборки
batch_size = 16

if K.image_data_format() == 'channels_first':
    # разменрность тензора на основе изображения для входных данных в нейронную сеть
    # backrnd Transflow, channels last
    input_shape = (3, img_width, img_height)
else:
    input_shape = (img_width, img_height, 3)

# сверточная нейронная сеть 
model = Sequential()

# каскады свертки и подвыборки, отвечают за выделение важных признаков изображений
model.add(Conv2D(32, (2, 2), input_shape=input_shape))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(32, (2, 2)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(64, (2, 2)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

# классификация
# преобразование двухмерного вывода из слоя MaxPooling в одномерный вектор
model.add(Flatten())

# полносвязный слой с 64 нейронами
model.add(Dense(64))

# полулинейная функция активации
model.add(Activation('relu'))

# слой для регуляризации и предотвращения обучения
model.add(Dropout(0.5))

# выходной слой с одним нейроном
model.add(Dense(1))

# сигмоидальная функция активации 
model.add(Activation('sigmoid'))

# компиляция сети
model.compile(
    
    # функция ошибки, бинарная, так как у нас два класса
    loss='binary_crossentropy',

    # тип оптимизатора
    optimizer='rmsprop',

    # тип метрики
    metrics=['accuracy']
    )

train_datagen = ImageDataGenerator(
    rescale=1. / 255,
    )

# IDG - класс, который загружает и может преобразовывать изображения 
# делим каждый пиксель изображения на 255
test_datagen = ImageDataGenerator(
    rescale=1. / 255
    )

# FFD - метод. Загрузка из каталога, в котором лежат наши файлы
train_generator = train_datagen.flow_from_directory(

    # каталог
    train_data_dir,

    # размер изображений
    target_size=(img_width, img_height),

    # количество изображений, которые будут прочитаны за один раз
    batch_size=batch_size,
    
    # режим класса, так как у нас два варианта, он готовит ответ 1 или 0
    class_mode='binary')

# тоже самое, только для проверки
validation_generator = test_datagen.flow_from_directory(
    validation_data_dir,
    target_size=(img_width, img_height),
    batch_size=batch_size,
    class_mode='binary')

# начинаем обучение нейронной сети
model.fit(

    # генератор данных для обучения
    train_generator,

    # количество изображений деленное на размер выборки, то есть используем одно изображение один раз
    # иначе генератор будет возвращаться к первому изображению и начинать все заново
    steps_per_epoch=nb_train_samples // batch_size,
    epochs=epochs,

    # генератор данных для проверки
    validation_data=validation_generator,
    validation_steps=nb_validation_samples // batch_size)

# сохраняем модель нейронной сети
model.save('neuronet/model_saved.h5')

from process_img import process_img
 
res, label_f = process_img('user_photo.jpg')

print(f'{res} {label_f}')


